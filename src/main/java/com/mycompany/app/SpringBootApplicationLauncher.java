package com.mycompany.app;

import org.springframework.boot.SpringApplication;
/**
 * This Launcher for the spring boot application.
 * 
 * @author manoj.bardhan
 *
 */
public class SpringBootApplicationLauncher {
    public static void main(String[] args) {
        SpringApplication.run(HelloWorldController.class, args);
    }
}