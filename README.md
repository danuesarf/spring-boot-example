# Spring boot bitbucket pipeline example
##### This example is to demonstrate how to use bitbucket-pipeline to deploy simple springboot application

## How does it works
##### The bitbucket-pipelines.yml will authenticate to Kubernetes using certificate and private key, and will run the kubectl command to change the image. If the changes are detected, it will automatically run the pipeline, and rebuild the application and push it to ECR with tag format of: branch_commitID_BitbucketPipelinebuildNumber.



## Required Environment Variables
##### To start using this pipeline you need to setup the following Environment Variable in the Bitbucket (Accessible from Repository > Settings > Pipelines > Repository Variables)

### AWS ECR Credentials
##### AWS_DEFAULT_REGION = Region of the ECR
##### AWS_ACCESS_KEY_ID = Access key to access ECR
##### AWS_SECRET_ACCESS_KEY = Secret key to access ECR
##### AWS_REGISTRY_URL = URI of the ECR

### Kubernetes Credentials
##### KUBE_CRT_BASE64 = Base64 encoded of the kubernetes client.crt
##### KUBE_KEY_BASE64 = Base64 encoded of the kubernetes client.key
##### KUBE_CONFIG_BASE64 = Base64 encoded of the ~/.kube/config

### Application
##### APPLICATION_NAME    = Application name

## Generate Base64
##### to Generate base64, simply find your working kube config file then as an example run: `cat ~/.kube/config | base64`, it will return the base64 encoded based on your configuration file, you can do the same thing with crt and key.